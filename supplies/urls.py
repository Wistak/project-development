from django.urls import path, include
from . import views 


urlpatterns = [
    path('', views.indexView, name='index'),
    path('detail/<int:pk>/', views.detailView, name='detail'),
    path('edit/<int:pk>/', views.editView, name='edit'),
    path('insumo/', views.insumoView, name='insumo'),
    path('borrar/<int:pk>/', views.deleteView, name='borrar'),
    path('pedidos/', views.pedidosIndexView, name="pedidos"),
    path('pedidos/detalle/<int:pk>/', views.pedidoDetailView, name="pedidoDetalle"),
]

from django.db import models


class Sucursal(models.Model):
    nombre = models.CharField(max_length=120)

    def __str__(self):
        return self.nombre


class Articulo(models.Model):
    nombre = models.CharField(max_length=120, help_text='Nombre o descripción')
    descripcion = models.TextField()
    precio = models.FloatField(default=0.0)

    def __str__(self):
        return self.nombre


class Inventario(models.Model):
    sucursal = models.ForeignKey(Sucursal, on_delete=models.CASCADE)
    articulo = models.ForeignKey(Articulo, on_delete=models.CASCADE)
    cantidad = models.IntegerField(default=0)

    def __str__(self):
        return str(self.sucursal) + " - " + str(self.articulo)

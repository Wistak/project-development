from django.shortcuts import render, redirect, get_object_or_404
from .forms import SucursalForm, ArticuloForm, InventarioForm
from .models import Sucursal, Articulo, Inventario
from django.views.generic import ListView, DetailView, TemplateView
from django.http import HttpResponse
import requests, json, sys



# SUPPLIES #

def indexView(request):
	r = requests.get('http://localhost:8000/api/')
	if r.status_code == 200:
		insumos = r.json()

	return render(request, "supplies/index.html", {"insumos": insumos})


def detailView(request, pk):
	r = requests.get('http://localhost:8000/api/detalle/' + str(pk))
	if r.status_code == 200:
		insumos = r.json()
	return render(request, "supplies/post-detail.html", {"insumo": insumos})



def insumoView(request):
	if request.method == 'POST':
		form = ArticuloForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect('index')
	form = ArticuloForm()
	return render(request,'supplies/post.html',{'form': form}) 


def editView(request, pk, template_name='edit.html'):
    r = requests.get('http://localhost:8000/api/detalle/' + str(pk))
    (pk)
    if r.status_code == 200:
        insumos = r.json()
    return render(request, "supplies/edit.html", {"insumo": insumos})
    #form = ArticuloForm(request.POST or None, instance=post)
    #if form.is_valid():
    #    form.save()
    #    return redirect('index')
    #return render(request, template_name, {'form':form})


def deleteView(request, pk, template_name='confirm_delete.html'):
    r = requests.get('http://localhost:8000/api/detalle/' + str(pk))
    (pk)
    if r.status_code == 200:
        insumos = r.json()
    return render(request, "supplies/confirm_delete.html", {"insumo": insumos})


# INVOICES #

def pedidosIndexView(request):
	r = requests.get('http://localhost:8000/api/pedidos/')

	if r.status_code == 200:
		pedido = r.json()
	pedido_str = json.dumps(pedido)
	pedido_arr = json.loads(pedido_str)
	
	r2 = requests.get('http://localhost:8000/api/sucursales/detalle/' + str(pedido_arr[0]['sucursal']))
	r3 = requests.get('http://localhost:8000/api/detalle/' + str(pedido_arr[0]['insumo']))

	if r2.status_code == 200:
		sucursal = r2.json()
	
	if r3.status_code == 200:
		insumo = r3.json()

	return render(request, "pedidos/index.html", { "pedidos": pedido, "sucursales":  sucursal, "insumos": insumo })


def pedidoDetailView(request, pk):
	r = requests.get('http://localhost:8000/api/pedidos/detalle/' + str(pk))
	if r.status_code == 200:
		pedidos = r.json()
	return render(request, "pedidos/post-detail.html", {"pedido": pedidos})
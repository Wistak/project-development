from django import forms
from .models import Sucursal, Articulo, Inventario

class SucursalForm(forms.ModelForm):
    class Meta:
        model = Sucursal
        fields = "__all__"


class ArticuloForm(forms.ModelForm):
    class Meta:
        model = Articulo
        fields = "__all__"


class InventarioForm(forms.ModelForm):
    class Meta:
        model = Inventario
        fields = "__all__"

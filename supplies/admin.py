from django.contrib import admin

from .models import Sucursal, Articulo, Inventario

admin.site.register(Sucursal)
admin.site.register(Articulo)
admin.site.register(Inventario)

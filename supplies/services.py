import requests

def get_insumos():
    url = 'http://localhost:8000/api/'
    response = requests.get(url)
    insumos = response.json()
    insumos_list = []
    for i in range(len(insumos)):
        insumos_list.append(insumos[i])
    return insumos_list
